import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user';
import { tap, switchMap } from "rxjs/operators";
import { Observable, BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';



@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private url = environment.apiUrl;
  public user: BehaviorSubject<User> = new BehaviorSubject(null);

  constructor(private http: HttpClient, private router: Router) { }

  addUser(user: User) {
    return this.http.post<User>(this.url + 'user/save', user);
  }
  login(mail: string, password: string) {
    return this.http.post<{ token: string }>(this.url + 'login', {
      mail,
      password
    }).subscribe((res) => {
      this.user.next(res['user'])
      localStorage.setItem('token', res.token)
      sessionStorage.setItem('token', res.token)

    })
  }

  getToken(): string {
    return localStorage.getItem('token');
  }

  logout() {
    localStorage.removeItem('token');
    this.user.next(null);
  }

  getUser(): Observable<User> {
    return this.user
  }
}
