import { ObjectId } from "mongodb";
import { User } from "./user";

export interface Event {
    _id?: ObjectId;
    name: string;
    startEvent: Date;
    endEvent: Date;
    location: string;
    author: User;
    details: string;
}
export class Event {
    constructor(data: Event = null) {
        this._id = data && data._id ? data._id : new ObjectId()
        if (data && data.name) this.name = data.name
        if (data && data.startEvent) this.startEvent = data.startEvent
        if (data && data.endEvent) this.endEvent = data.endEvent
        if (data && data.location) this.location = data.location
        if (data && data.author) this.author = data.author
        if (data && data.details) this.details = data.details
    }
}