import { ObjectId } from "mongodb";
import * as bcrypt from 'bcrypt';

export interface User {
    _id?: ObjectId;
    name: string;
    firstName: string;
    pseudo: string;
    mail: string;
    password?: any;
    password2?: any;
    roles?: string;
}

const regexpPass = new RegExp(/^(?=.{8,}$)(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*\W).*$/)

export class User implements User {
    constructor(name: string = null, firstName: string = null, pseudo: string = null, mail: string = null, data = null) {

        if (data && data._id && ObjectId.isValid(data._id))
            this._id = typeof data._id == 'string' ? new ObjectId(data._id) : data._id
        else this._id = new ObjectId()
        this._id = data && data._id ? data._id : new ObjectId()
        this.name = data && data.name ? data.name : name
        this.firstName = data && data.firstName ? data.firstName : firstName
        this.pseudo = data && data.pseudo ? data.pseudo : pseudo
        this.mail = data && data.mail ? data.mail : mail
        if (data && data.password) this.password = data.password
        if (data && data.password2) this.password2 = data.password2
        if (data && data.roles) this.roles = data.roles
    }

    async setPassword(password, salt = false) {
        try {
            if (!regexpPass.test(password)) throw new Error('Invalid password')
            this.password = salt == true ? await getSalt(password) : password
        } catch (err) {
            return err
        }
    }

}

function getSalt(password) {
    return new Promise(async function (resolve, reject) {
        try {
            await bcrypt.hash(password, 10, async function (err, hash) {
                if (err) throw err
                resolve(hash)
            });
        } catch (err) {
            reject(new Error(err))
        }
    })
}