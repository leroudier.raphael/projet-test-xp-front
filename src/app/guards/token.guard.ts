import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';
import jwt from "jwt-decode";

/**
 * Les Guards sont des services spéciaux de angular qui seront utilisé
 * pour indiquer si un user à accès à une route ou non.
 * Ils implémentent une méthode canActivate qui doit renvoyer du booléen,
 * si renvoie true, le user à accès à la page, si false, l'accès est refusé.
 * L'idée sera de faire des conditions pour renvoyer true ou false selon
 * certaines valeurs (présence ou non d'un token, contrainte temporelle)
 */


@Injectable({
  providedIn: 'root'
})
export class TokenGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    //Ici, on récupère le token et on vérifie s'il existe
    if (this.authService.getToken()) {
      //Si oui, on décode le jwt pour avoir accès à son contenu
      let token: any = jwt(this.authService.getToken());
      //On renvoie true/false selon si le token est expiré ou non
      if (token.exp > new Date().getTime() / 1000) {
        return true;
      }
    }
    //Si pas de token ou token expiré, on redirige le user vers la route landing
    this.router.navigate(['/login']);
    return false;
  }

}
