import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { ErrorStateMatcher } from '@angular/material/core';
import { environment } from 'src/environments/environment';
import { User } from '../../models/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

/** Error when invalid control is dirty, touched, or submitted. */

export class LoginComponent implements OnInit {
  hide: boolean = true
  loginForm = this.fb.group({
    mail: [null, [Validators.required, Validators.email]],
    password: [null,
      [Validators.required, Validators.pattern(/^(?=.{8,}$)(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*\W).*$/)]]
  });
  matcher = new MyErrorStateMatcher();
  user
  constructor(private fb: FormBuilder, public httpClient: HttpClient, private router: Router, private authService: AuthService) { }
  ngOnInit() {
    if (this.authService.getUser()) { this.router.navigate(['/home']) }
  }
  login() {
    try {
      if (this.loginForm.valid) {
        this.authService.login(this.loginForm.value.mail, this.loginForm.value.password)
        this.user = this.authService.getUser()
        this.router.navigate(['/home'])
    
      }
    } catch (err) {
      console.error(err);
    }
  }

}
class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}